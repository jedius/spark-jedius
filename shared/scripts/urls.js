/*globals window:false*/
(function(exports) {
    "use strict";
    exports.searchUrl = function (country, category, order, page) {
        var hasOrder = order && order !== 'funding';
        var hasPage = page > 1;
        return '/change-maker/' +
            (country ? 'country/' : category ? 'category/' : '') +
            (country ? country + '/' : '' ) +
            (category ? category + '/' : '' ) +
            (hasOrder ? '?order=' + order : '' ) +
            (hasPage ? (hasOrder ? '&' : '?') + 'page=' + page : '');
    };
}(typeof module !== 'undefined' && module.exports || (window.Spark = {})));
