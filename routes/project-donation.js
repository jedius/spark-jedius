"use strict";
var async = require('async');
var EventEmitter = require('events').EventEmitter;
var deepEquals = require('deep-equal');
var _ = require('lodash');

var braintree = require('../lib/donation/braintree');
var validate = require('../lib/donation/validation');
var recordDonation = require('../lib/donation/record-donation');
var emailReceipt = require('../lib/donation/email-receipt');

var db = require('../lib/db');

function emailUser(project, user, transaction, next) {
    emailReceipt(project, user, transaction, function(err) {
        if (err) {
            return next(err);
        }
        next(null, true);
    });
}

function donate(req, res, next) {
    var emitter = new EventEmitter();

    var project;

    var doValidation = validate.bind(null, req, function(errors) {
        if (errors) {
            return emitter.emit('userError', errors);
        }
        // Ensure the project exists as well
        db.ready(function(api) {
            api.project.get(req.body.project_slug, function(err, foundProject) {
                if (err) {
                    return emitter.emit('error', err);
                }
                if (!foundProject) {
                    return emitter.emit('userError', [ 404, 'Project not found']);
                }
                project = foundProject;
                emitter.emit('validatedReq', req);
            });
        });
    });


    /*
     * If they're not logged in, we create a user that can't log in externally...and 
     * then log them in.
     * They will stay logged in as long as their session stays alive.
     * After that, the user won't have any more activity.
     * The goal is just to make sure if they double submit, we don't create dupe users.
     */
    function ensureUser(req) {
        function next(err) {
            if (err) {
                emitter.emit('error', err);
                return;
            }
            emitter.emit('authenticatedReq', req);
        }

        db.ready(function(api) {
            var fields = req.body;
            var user = req.user;
            if (!user) {
                user = api.user.blank();
                user.first_name = fields.first_name;
                user.last_name = fields.last_name;
                user.email = fields.email;
                user.is_anonymous = fields.anonymity !== 'public';
                user.receives_notifications = ~fields.updates.indexOf('spark');
                user.is_registered = false;
            }

            var newAddress = {
                address1 : fields.address1,
                address2 : fields.address2,
                city : fields.city,
                state : fields.state,
                country : fields.country,
                postcode : fields.postcode
            };
            if (!user.addresses) {
                user.addresses = [];
            }
            if (!_.find(user.addresses, newAddress, deepEquals)) {
                user.addresses.unshift(newAddress);
                if (user.addresses.length > 50) {
                    user.addresses.pop();
                }
            }

            api.user.save(user, function(err, savedUser) {
                if (err) {
                    return next(err);
                }
                req.user = savedUser;
                next();
                // if (!req.user) {
                //     req.logIn(savedUser, next);
                // } else {
                //     next();
                // }
            });
        });
    }

    function doBraintreeTransaction(req) {
        braintree.makeDonationRequest(req.body, function(err, result) {
            if (err) {
                return emitter.emit('error', err);
            }

            switch(result.status) {
                case braintree.status.SUCCESS:
                    return emitter.emit('transactionSucceeded', req, result.transaction);
                case braintree.INVALID:
                    return emitter.emit('userError', {
                        invalid : result.errors
                    });
                case braintree.status.DECLINED:
                    console.log('Declined: ', result.transaction);
                    return emitter.emit('userError', 'Your donation could not be processed.');
                case braintree.status.UNRECOGNIZED:
                    return emitter.emit('error', result.data);
                default:
                    return emitter.emit('error', result);
            }
        });
    }

    function recordDonationAndEmailUser(req, transaction) {
        var tempUserInfo = {
            first_name : req.body.first_name,
            last_name : req.body.last_name,
            email : req.body.email
        };
        emailUser(project, tempUserInfo, transaction, function(err, receiptEmailed) {
            if (err) {
                // log, but don't stop on email errors.
                // the transaction went through but isn't recorded.
                // this is a bad spot to be in, so let's try to record it anyway.
                console.error('Error sending email for ' + transaction.id + ':\n' +
                    JSON.stringify(err));
            }

            recordDonation(req.user, project, transaction,
                req.body.anonymity !== 'public',
                ~req.body.updates.indexOf('changemaker'),
                receiptEmailed,
                function(err, grantInfo) {
                    if (err) {
                        return emitter.emit('error', err);
                    }
                    emitter.emit('transactionRecorded', grantInfo);
                });
        });
    }

    // begin the process
    process.nextTick(function(){
        emitter.emit('req', req);
    });

    // 1. validate form
    emitter.on('req', doValidation);

    // 2. ensure there is a user record for this person.
    emitter.on('validatedReq', ensureUser);

    // 3. braintree transaction
    emitter.on('authenticatedReq', doBraintreeTransaction);

    // 5. update project - add contribution, possibly set attained date
    emitter.on('transactionSucceeded', recordDonationAndEmailUser);

    // 6a. respond to user on success - send new grant info/attained/percent-left
    emitter.on('transactionRecorded', function respondWithSuccess(grantinfo) {
        res.send(200, grantinfo);
    });

    // 6b. respond to user on fail with error message
    emitter.on('error', function respondWithError(err) {
        console.error(err);
        res.send(500, 'An error occurred while processing your request.');
    });

    emitter.on('userError', function respondWithUserError(err) {
        console.error(err);
        if (err instanceof Array) {
            return res.send.apply(res, err);
        }
        res.send(400, err);
    });
}
module.exports.donate = donate;
