var db = require('../../lib/db');
var async = require('async');
var slugs = require('slugs');

function sumOfValues(obj) {
    var sum = 0;
    for(var prop in obj) {
        sum += obj[prop];
    }
    return sum;
}

function asSluggedKeyValue(namesObj) {
    return Object.keys(namesObj).map(function(name) {
        return { key : slugs(name), value : name };
    });
}

module.exports = function() {
    return function(req, res, next) {
        db.ready(function(api) {
            async.parallel([
                api.project.locations,
                api.project.categories,
                api.changemaker.count
            ], function(err, results) {
                if (err) {
                    return next(err);
                }

                var countries = asSluggedKeyValue(results[0]);
                var categories = asSluggedKeyValue(results[1]);
                var projectCount = sumOfValues(results[0]);
                var changemakerCount = results[2];

                res.locals.projectCount = projectCount;
                res.locals.changemakerCount = changemakerCount;
                res.locals.countries = countries;
                res.locals.categories = categories;
                res.locals.csrfToken = req.session._csrf;

                next();
            });
        });
    };
};
