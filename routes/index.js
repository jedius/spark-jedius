"use strict";
/*
 * GET home page.
 */
var projectRoutes = require("./project");
var userRoutes =  require("./user");
var authRoutes = require("./auth");
var express = require('express');

var db = require("../lib/db");

function renderIndexPage(req, res, next) {
    var viewBag = {
        title: "Spark* - 1 Million People out of Extreme Poverty",
        atHome: true
    };
    res.render('index', viewBag);
}
module.exports.index = renderIndexPage;

function renderLoginPage(req, res, next) {
    res.render('login');
}
module.exports.login = renderLoginPage;

function renderStaticPrivacyPolicy(req, res, next) {
    "use strict";
    var viewBag = {
        title: "Spark* Privacy Policy"
    };
    res.render('privacyPolicy', viewBag);
}

function renderStaticTermsConditions(req, res, next) {
    "use strict";
    var viewBag = {
        title: "Spark* Terms and Conditions"
    };
    res.render('termsConditions', viewBag);
}

function renderSparkInfoPage(req, res, next) {
    var viewBag = {
        title: "Spark*"
    };
    res.render('spark', viewBag);
}

function renderSupporterInfoPage(req, res, next) {
    var viewBag = {
        title: "You"
    };
    res.render('you', viewBag);
}

function renderOneMillionPage(req, res, next) {
    var viewBag = {
        title: "1 Million People Out of Poverty"
    };
    res.render('oneMillion', viewBag);
}

function notImplementedRoute(req, res, next) {
    return next(new Error("This route is not implemented"));
}

function redirectToFilterPage(req, res, next) {
    res.redirect("/change-maker");
}

function redirectToHttps(req, res, next) {
    if(req.headers['x-forwarded-proto'] != 'https') {
        res.redirect('https://www.sparkinternational.org'+req.url);
    } else {
        next();
    }
}

function setupRoutes(app) {
    app.get("*", redirectToHttps);
    app.get("/", renderIndexPage);
    //app.get("/login", renderLoginPage);
    app.get("/spark", renderSparkInfoPage);
    app.get("/you", renderSupporterInfoPage);
    app.get("/1-million-people", renderOneMillionPage);
    app.get("/change-maker", projectRoutes.list);
    app.get("/change-maker/country", redirectToFilterPage);
    app.get("/change-maker/country/:countrySlug", projectRoutes.listByCountry);
    app.get("/change-maker/category", redirectToFilterPage);
    app.get("/change-maker/category/:categorySlug", projectRoutes.listByCategory);
    app.get("/change-maker/country/:countrySlug/:categorySlug", projectRoutes.listByCountryAndCategory);
    app.get("/change-maker/:projectSlug", projectRoutes.getProject);
    app.post("/change-maker/:projectSlug/donate", projectRoutes.donation.donate);
    app.get("/supporter/:userId", notImplementedRoute, userRoutes.userProfile);
    app.get("/privacy-policy", renderStaticPrivacyPolicy);
    app.get("/terms-and-conditions", renderStaticTermsConditions);

    // authentication routes
    require("./auth").addAuthRoutes(app);
}
module.exports.setup = setupRoutes;
