"use strict";

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: '<json:package.json>',
    test: { // Nodeunit tests for server-side code
      files: ['test/server/**/*-test.js', 'test/server-integration/**/*-test.js']
    },
    qunit: { // QUnit tests for client-side code
      files: ['test/client/**/*.html']
    },
    lint: {
      files: [
        'grunt.js',
        'index.js',
        'lib/**/*.js',
        'routes/**/*.js',
        'shared/**/*.js',
        'public/scripts/**/*.js',
        'test/server/**/*.js',
        'test/client/**/*.js'
      ]
    },
    watch: {
      js : {
        files: '<config:lint.files>',
        tasks: 'process-js'
      }
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        node: true
      },
      globals: {
        exports: true,
        jQuery:false,

        // QUnit globals
        QUnit:false, module:false, test:false, asyncTest:false, expect:false,
        start:false, stop:false, ok:false, equal:false, notEqual:false, deepEqual:false,
        notDeepEqual:false, strictEqual:false, notStrictEqual:false, raises:false
      }
    }
  });
  grunt.loadTasks('tasks');

  grunt.registerTask('test-client', 'qunit');

  grunt.registerTask('process-js', 'lint test test-client');

  // Default task.
  grunt.registerTask('default', 'process-js');

};
