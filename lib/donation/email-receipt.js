var config = require("../../config.js");
var email = require("../email");
var fs = require('fs');
var path = require('path');

var receiptTemplatePath = path.join(__dirname, '..', '..', 'views', 'email', 'receipt.txt');
var receiptTemplate = fs.readFileSync(receiptTemplatePath, 'utf8');

function renderReceiptTemplate(project, user, transaction) {
    return receiptTemplate.replace(/%(\w+)%/g, function(str) {
        var varName = str.substring(1, str.length - 1);
        switch(varName) {
            case 'USER_NAME': return user.first_name;
            case 'AMOUNT': return transaction.amount;
            case 'PROJECT_NAME': return project.name;
            case 'TRANSACTION_ID': return transaction.id;
            case 'TRANSACTION_DATE': return new Date(transaction.createdAt).toDateString();
        }
    });
}

function formatName(plain, email) {
    return plain + " <" + email + ">"
}

var from = formatName(config.emailSmtp.fromName, config.emailSmtp.fromAddress);

module.exports = function emailReceipt(project, toUser, transaction, next) {
    email.sendMail({
        from: from,
        to: formatName(toUser.first_name + ' ' + toUser.last_name, toUser.email),
        subject: "Thank you for your donation!",
        text: renderReceiptTemplate(project, toUser, transaction),
        html: null
    }, next);
};
