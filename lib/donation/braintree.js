"use strict";

var braintree = require('braintree');

var config = require('../../config');

var braintreeOptions = {
    environment : config.environment === 'production' ?
        braintree.Environment.Production :
        braintree.Environment.Sandbox,
        //braintree.Environment.Development, // localhost:3000
    merchantId : config.braintree.merchantId,
    publicKey : config.braintree.publicKey,
    privateKey : config.braintree.privateKey
};

var gateway = braintree.connect(braintreeOptions);

if (braintreeOptions.environment === braintree.Environment.Development) {
    require('./braintree-dev-server')();
}

var status = module.exports.status = {
    SUCCESS : 'success',
    INVALID : 'validationError',
    DECLINED : 'declinedError',
    UNRECOGNIZED : 'unrecognizedError'
};

function pluckMessage(err) {
    return err.message;
}

// undo the dumbass structure of braintree errors.
function extractErrorMessages(error) {
    var key;
    var ret = {};

    if (error.validationErrors) {
        for (key in error.validationErrors) {
            ret[key] = error.validationErrors[key].map(pluckMessage);
        }
    }

    if (error.errorCollections) {
        for (key in error.errorCollections) {
            ret[key] = extractErrorMessages(error.errorCollections[key]);
        }
    }

    return ret;
}

/**
 * Accepts an object with donation fields and talks to Braintree to
 * make the donation.
 */
module.exports.makeDonationRequest = function(donationFields, next) {
    var saleRequest = {
        amount: donationFields.fund_amount.toFixed(2),
        creditCard: {
            number            : donationFields.number,
            cvv               : donationFields.cvv,
            expirationMonth   : donationFields.month,
            expirationYear    : donationFields.year
        },
        billing: {
            firstName         : donationFields.first_name,
            lastName          : donationFields.last_name,
            streetAddress     : donationFields.address1,
            extendedAddress   : donationFields.address2,
            locality          : donationFields.city,
            region            : donationFields.state,
            postalCode        : donationFields.postcode,
            countryCodeAlpha2 : donationFields.country
        },
        options: {
            submitForSettlement: true
        }
    };

    gateway.transaction.sale(saleRequest, function(err, result) {
        if (err) {
            return next(new Error(JSON.stringify({
                request : saleRequest,
                error: err
            })));
        }
        if (result.success) {
            return next(null, {
                status : status.SUCCESS,
                transaction : result.transaction
            });
        }

        if (!result.transaction) { // validation error
            
            var errors = extractErrorMessages(result.errors.errorCollections.transaction);
            var creditCardErrors = errors.creditCard || {};
            var billingErrors = errors.billing || {};

            return next(null, {
                status : status.INVALID,
                errors : {
                    fund_amount : errors.amount,
                    number      : creditCardErrors.number,
                    cvv         : creditCardErrors.cvv,
                    month       : creditCardErrors.expirationMonth,
                    year        : creditCardErrors.expirationYear,
                    first_name  : billingErrors.firstName,
                    last_name   : billingErrors.lastName,
                    address1    : billingErrors.streetAddress,
                    address2    : billingErrors.extendedAddress,
                    city        : billingErrors.locality,
                    state       : billingErrors.region,
                    postcode    : billingErrors.postalCode,
                    country     : billingErrors.countryCodeAlpha2
                }
            });
        }

        switch(result.transaction.status) {
            case 'processor_declined': // card company didn't like it
            case 'gateway_rejected': // our AVS or CVV setup at Braintree didn't like it.
                return next(null, {
                    status : status.DECLINED,
                    transaction: result.transaction
                });
        }

        return next(null, {
            status : status.UNRECOGNIZED,
            data : result
        });
    });
};
