"use strict";

var _ = require('lodash');

function getAttainedDate(grant) {
    return grant.attained_date;
}
function getUnattained(grant) {
    return !grant.attained_date;
}

function getCurrentGrant(project) {
    // get the first grant that is not attained, or if they're all attained, the 
    // one that was attained most recently.
    return _.find(project.funding_grants, getUnattained) ||
            _.max(project.funding_grants, getAttainedDate);
}

exports.getCurrentGrant = getCurrentGrant;
