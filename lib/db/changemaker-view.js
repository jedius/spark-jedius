    /*jshint strict:false*/
/*globals emit,reduce,sum*/
module.exports = {
    _id: "changemaker",
    language: "javascript",
    views: {
        count : {
            map: function(doc) {
                if(doc.type === 'changemaker') {
                    emit(null, 1);
                }
            },
            reduce: function(key, values) {
                return sum(values);
            }
        }
    }
};
