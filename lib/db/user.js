module.exports = function(sparkdb) {
    "use strict";

    // e.g. twitter, sugendran, func
    function getUserByAuth(providerName, providerId, callback) {
        providerName = providerName.toLowerCase();
        providerId = providerId.toLowerCase();
        var params = {
            key: [providerName, providerId],
            limit: 1
        };
        sparkdb.view('user', 'byAuth', params, function(err, doc) {
            if(err) {
                return callback(err);
            }
            var user = (doc.rows && doc.rows.length ? doc.rows[0].value : null);
            callback(null, user);
        });
    }

    function saveUser(user, callback) {
        //TODO: validation
        if(!user.type || user.type !== 'user') {
            return callback(new Error("user.type is not set correctly."));
        }
        user.modified_date = Date.now();
        console.log("saving user %j", user);
        sparkdb.insert(user, function(err, body) {
            if(err) {
                console.log("error saving user: %j", err);
                return callback(err);
            }
            user._id = body.id;
            callback(null, user);
        });
    }

    //TODO: can we cache this in redis?
    //      it seems that passportjs hits
    //      it on every page. [not urgent]
    function getUser(userId, callback) {
        sparkdb.get(userId, callback);
    }

    function getUserbyEmail(email, callback) {
        var params = {
            key: email,
            limit: 1
        };
        sparkdb.view('user', 'byEmail', params, function(err, doc) {
            if(err) {
                return callback(err);
            }
            var user = (doc.rows && doc.rows.length ? doc.rows[0].value : null);
            callback(null, user);
        });
    }

    function getUsersByLastModified(lastModifiedTS, callback) {
        var params = {
            startkey: lastModifiedTS
        };
        sparkdb.view('user', 'byLastModified', params, function(err, doc) {
            if(err) {
                return callback(err);
            }
            var users = (doc.rows ? doc.rows : []);
            callback(null, users);
        });
    }

    function blankUser() {
        return {
            "type": "user",
            "salesforce_ref" : "",
            "first_name": "",
            "last_name": "",
            "email": "",
            "addresses" : [],
            "created_date": Date.now(),
            "modified_date": Date.now(),
            "is_registered": false,
            "is_anonymous": false,
            "receives_notifications" : true,
            "authentication": [],
            "avatar_url": null,
            "searches": [],
            "activities": []
        };
    }

    return {
        getByAuth: getUserByAuth,
        getByEmail: getUserbyEmail,
        getByLastModified: getUsersByLastModified,
        save: saveUser,
        blank: blankUser,
        get: getUser
    };
};
