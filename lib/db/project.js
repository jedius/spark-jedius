var slugs = require("slugs");
var _ = require("lodash");

module.exports = function(sparkdb) {
    "use strict";

    var changemakerApi = require('./changemaker')(sparkdb);
    var acceleratorApi = require('./changemaker')(sparkdb);

    function listProjectIds(callback) {
        sparkdb.view('project', 'allIds', function(err, doc) {
            if(err) {
                return callback(err);
            }
            var keys = _.map(doc.rows, "key");
            callback(null, keys);
        });
    }

    function projectIdsWithContributionsSince(startDate, callback) {
        sparkdb.view('project', 'lastContributionDate', { startkey: startDate },
            function(err, doc) {
                if(err) {
                    return callback(err);
                }
                var keys = _.map(doc.rows, "value");
                callback(null, keys);
            });
    }

    function listCategories(callback) {
        sparkdb.view('project', 'countsByCategory', { group_level:1 }, 
            function(err, doc) {
                if(err) {
                    return callback(err);
                }
                var result = {};
                doc.rows.forEach(function(r) {
                    result[r.key] = r.value;
                });
                callback(null, result);
            });
    }

    function listLocations(callback) {
        sparkdb.view('project', 'countsByLocation', { group_level:1 }, 
            function(err, doc) {
                if(err) {
                    return callback(err);
                }
                var result = {};
                doc.rows.forEach(function(r) {
                    result[r.key] = r.value;
                });
                callback(null, result);
            });
    }

    function getSearchKeyPrefix(location, category) {
        var key = [ true ]; // only get projects that are seeking funding.
        if (location) {
            key.push(0);
            key.push(location);
        }
        if (category) {
            key.push(1);
            key.push(category);
        }

        key.push(2);

        return key;
    }

    var sortOrders = {
        goalDistance     : { name : 'ByFunding', desc : false },
        creationDateDesc : { name : 'ByCreationDate', desc : true },
        recentUpdateDesc : { name : 'ByRecentUpdate', desc : true }
    };

    function searchProjects(filters, callback) {
        if (!sortOrders.hasOwnProperty(filters.order)) {
            return callback(new Error("Invalid sort order " + filters.order));
        }

        var sortOrder = sortOrders[filters.order];
        var start = filters.start || 0;
        var limit = filters.limit || 4;
        var keyPrefix = getSearchKeyPrefix(filters.location, filters.category);
        
        var viewName = "byFiltersThen" + sortOrder.name;
        var params = {
            descending : sortOrder.desc,
            startkey   : sortOrder.desc ? keyPrefix.concat({}) : keyPrefix,
            endkey     : sortOrder.desc ? keyPrefix            : keyPrefix.concat({}),
            skip : start * 2,
            include_docs : true
        };
        sparkdb.view('project', viewName, params,
            function(err, doc) {
                if (err) {
                    return callback(err);
                }
                if (doc.rows && doc.rows.length) {
                    var projects = [];
                    var rowHandler;

                    // if we're descending, we have to preserve the changemaker for the following project.
                    // otherwise, we add the changemaker to the previous project.
                    if (sortOrder.desc) {
                        var lastChangemaker;
                        rowHandler = function(row) {
                            switch (row.doc.type) {
                                case 'project':
                                    row.doc.changemaker = lastChangemaker;
                                    projects.push(row.doc);
                                    break;
                                case 'changemaker':
                                    lastChangemaker = row.doc;
                                    break;
                                default:
                                    throw new Error("Unexpected doc type: " + row.doc.type);
                            }
                        };
                    } else {
                        var lastProject;
                        rowHandler = function(row) {
                            switch (row.doc.type) {
                                case 'project':
                                    lastProject = row.doc;
                                    projects.push(lastProject);
                                    break;
                                case 'changemaker':
                                    lastProject.changemaker = row.doc;
                                    break;
                                default:
                                    throw new Error("Unexpected doc type: " + row.doc.type);
                            }
                        };
                    }
                    doc.rows.slice(0, limit * 2).forEach(rowHandler);
                    
                    return callback(null, {
                        projects : projects,
                        total : Math.floor(doc.rows.length / 2) + start
                    });
                }
                callback(null, { projects : [], total : start });
            });
    }

    function getProject(slug, callback) {

        sparkdb.view('project', 'bySlugWithLinkedEntities', {
            include_docs: true,
            startkey : [ slug ],
            endkey : [ slug, {} ]
        },
            function(err, doc) {
                if (err) {
                    return callback(err);
                }

                if (!doc.rows || !doc.rows.length) {
                    return callback(null, null);
                }

                var project;
                var changemaker;
                var accelerator;
                var helpers = [];
                var funders = [];

                doc.rows.forEach(function(row) {
                    var doc = row.doc;

                    // TODO: remove this. it just avoids us showing bad data (nonexistant users)
                    if (!doc) {
                        return;
                    }
                    switch(row.key[1]) {
                        case 0:
                            project = doc;
                            break;
                        case 1:
                            doc.amount_usd = row.value.amount_usd;
                            doc.is_anonymous = doc.is_anonymous || row.value.is_anonymous;
                            funders.push(doc);
                            break;
                        case 2:
                            doc.is_anonymous = doc.is_anonymous || row.value.is_anonymous;
                            helpers.push(doc);
                            break;
                        case 3:
                            changemaker = doc;
                            break;
                        case 4:
                            accelerator = doc;
                            break;
                    }
                });

                project.helpers = helpers;
                project.funders = funders;
                project.changemaker = changemaker;
                project.accelerator = accelerator;
                callback(null, project);
            });
    }

    function saveProject(project, callback) {
        //TODO: validation
        if(!project.type || project.type !== 'project') {
            return callback(new Error("project.type is not set correctly."));
        }
        if (!project.name) {
            return callback(new Error("Project name is required."));
        }

        if (!project.changemaker && !project.changemaker_id) {
            return callback(new Error("Changemaker or changemaker_id is required."));
        }

        if (project.changemaker && project.changemaker_id && project.changemaker._id !== project.changemaker_id) {
            return callback(new Error("Changemaker and changemaker_id properties are out of sync."));
        }

        if (!project.slug) {
            project.slug = slugs(project.name);
        }

        project.modified_date = Date.now();
        
        changemakerApi.upsert(project.changemaker, function(err) {
            if(err) {
                return callback(err);
            }

            console.log("saving project %j", project);
            var documentKey = project._id;

            // remove any denormalizations
            delete project.helpers;
            delete project.funders;
            // @sugendran wanted to leave this in I think?
            // I don't mind. It'll be overridden on get anyway - Adam
            // delete project.changemaker;

            sparkdb.insert(project, documentKey, function(err, body) {
                if(err) {
                    console.log("error saving project: %j", err);
                    return callback(err);
                }
                project._id = body.id;
                callback(null, project);
            });
        });
    }

    function blankProject() {
        return {
            "type" : "project",
            "salesforce_ref" : "",
            "name" : "",
            "slug" : "",
            "created_date": Date.now(),
            "modified_date" : Date.now(),
            "category" : "",
            "status" :  "prelaunch",
            "mission" : "",
            "problem" : "",
            "solution" : "",
            "people" : "",
            "funding_grants" : [],
            "followers" : [],
            "messages" : [],
            "help_offers" : [],
            "media" : {},
            "updates" : [],
            "changemaker_id" : null,
            "accelerator_id" : null,
            "location" : ""
        };
    }

    function getProjectById(projectId, callback) {
        sparkdb.get(projectId, callback);
    }

    return {
        categories: listCategories,
        locations : listLocations,
        search : searchProjects,
        sortOrders : sortOrders,
        save : saveProject,
        get : getProject,
        getById: getProjectById,
        blank : blankProject,
        getIds: listProjectIds,
        getIdsForNewContributions: projectIdsWithContributionsSince
    };
};
