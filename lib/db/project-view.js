/*jshint strict:false*/
/*globals emit,reduce,sum*/
module.exports = {
    _id: "project",
    language: "javascript",
    views: {
        byCategory: {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit(doc.category, doc);
                }
            }
        },
        byFiltersThenByFunding: {
            map: function(doc) {
                if (doc.type === 'project') {
                    var goal = 0;
                    var contributions = 0;
                    var hasOpenGrant = false;
                    for (var grant in doc.funding_grants) {
                        if (!doc.funding_grants[grant].attained_date) {
                            goal = doc.funding_grants[grant].amount_usd;
                            contributions = 0;
                            for (var contribution in doc.funding_grants[grant].contributions) {
                                contributions += doc.funding_grants[grant].contributions[contribution].amount_usd;
                            }
                            hasOpenGrant = true;
                            break; // only allow one active goal for now
                        }
                    }
                    var left = goal - contributions;

                    emit([ hasOpenGrant, 0, doc.location, 1, doc.category, 2, left / goal, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 0, doc.location, 2,                  left / goal, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 1, doc.category, 2,                  left / goal, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 2,                                   left / goal, doc._id, 0 ], null);

                    emit([ hasOpenGrant, 0, doc.location, 1, doc.category, 2, left / goal, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 0, doc.location, 2,                  left / goal, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 1, doc.category, 2,                  left / goal, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 2,                                   left / goal, doc._id, 1 ], { _id : doc.changemaker_id });
                }
            }
        },
        byFiltersThenByCreationDate: {
            map: function(doc) {
                if (doc.type === 'project') {
                    var hasOpenGrant = false;
                    for (var grant in doc.funding_grants) {
                        if (!doc.funding_grants[grant].attained_date) {
                            hasOpenGrant = true;
                            break;
                        }
                    }

                    emit([ hasOpenGrant, 0, doc.location, 1, doc.category, 2, doc.created_date, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 0, doc.location, 2,                  doc.created_date, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 1, doc.category, 2,                  doc.created_date, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 2,                                   doc.created_date, doc._id, 0 ], null);

                    emit([ hasOpenGrant, 0, doc.location, 1, doc.category, 2, doc.created_date, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 0, doc.location, 2,                  doc.created_date, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 1, doc.category, 2,                  doc.created_date, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 2,                                   doc.created_date, doc._id, 1 ], { _id : doc.changemaker_id });
                }
            }
        },
        byFiltersThenByRecentUpdate: {
            map: function(doc) {
                if (doc.type === 'project') {
                    var hasOpenGrant = false;
                    for (var grant in doc.funding_grants) {
                        if (!doc.funding_grants[grant].attained_date) {
                            hasOpenGrant = true;
                            break;
                        }
                    }

                    var max_update;
                    for (var update in doc.updates) {
                        max_update = max_update > doc.updates[update].created_date ?
                            max_update :
                            doc.updates[update].created_date;
                    }

                    emit([ hasOpenGrant, 0, doc.location, 1, doc.category, 2, max_update, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 0, doc.location, 2,                  max_update, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 1, doc.category, 2,                  max_update, doc._id, 0 ], null);
                    emit([ hasOpenGrant, 2,                                   max_update, doc._id, 0 ], null);

                    emit([ hasOpenGrant, 0, doc.location, 1, doc.category, 2, max_update, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 0, doc.location, 2,                  max_update, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 1, doc.category, 2,                  max_update, doc._id, 1 ], { _id : doc.changemaker_id });
                    emit([ hasOpenGrant, 2,                                   max_update, doc._id, 1 ], { _id : doc.changemaker_id });
                }
            }
        },
        bySlugWithLinkedEntities : {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit([ doc.slug, 0 ], null);

                    //Linked Entities
                    for (var grant in doc.funding_grants) {
                        for (var contribution in doc.funding_grants[grant].contributions) {
                            var contributionObj = doc.funding_grants[grant].contributions[contribution];
                            emit([ doc.slug, 1 ], {
                                amount_usd : contributionObj.amount_usd,
                                is_anonymous : contributionObj.is_anonymous,
                                _id : contributionObj.user_id
                            });
                        }
                    }
                    for (var help in doc.help_offers) {
                        emit([ doc.slug, 2 ], {
                            _id : doc.help_offers[help].user_id
                        });
                    }
                    emit([ doc.slug, 3 ], { _id : doc.changemaker_id });
                    emit([ doc.slug, 4 ], { _id : doc.accelerator_id });
                }
            }
        },
        countsByCategory: {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit(doc.category, 1);
                }
            },
            reduce: function(key, values) {
                return sum(values);
            }
        },
        countsByLocation: {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit(doc.location, 1);
                }
            },
            reduce: function(key, values) {
                return sum(values);
            }
        },
        allIds: {
            map: function(doc) {
                if(doc.type === 'project') {
                    emit(doc._id, 1);
                }
            }
        },
        lastContributionDate: {
            map: function(doc) {
                if(doc.type === 'project') {
                    var lastDate = 0;
                    for (var grant in doc.funding_grants) {
                        for (var contribution in doc.funding_grants[grant].contributions) {
                            var contributionObj = doc.funding_grants[grant].contributions[contribution];
                            lastDate = Math.max(lastDate, contributionObj.transaction_date);
                        }
                    }
                    if(lastDate > 0) {
                        emit(lastDate, doc._id);
                    }
                }
            }
        }
    }
};
