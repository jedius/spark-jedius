    /*jshint strict:false*/
/*globals emit,reduce,sum*/
module.exports = {
    _id: "accelerator",
    language: "javascript",
    views: {
        // We don't really need this but...
        count : {
            map: function(doc) {
                if(doc.type === 'accelerator') {
                    emit(null, 1);
                }
            },
            reduce: function(key, values) {
                return sum(values);
            }
        }
    }
};
