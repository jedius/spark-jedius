var login = require("./login");
var _ = require('lodash');
var async = require('async');
var fs = require('fs');
var db = require("../lib/db");
var mapper = require("./mapper");
var oauth;
var sf;
var nforce = login.nforce;
var individualAccountId;
var recordTypePublicDonationId;
var recordTypeContactId;
var currentBudgetYearId;

var queries = JSON.parse(fs.readFileSync(__dirname + "/queries.json"));

/// sync lock stuff
var lastSyncInfo;
var syncStartDate;
function getLastSyncDate(callback) {
    console.log("Determining last sync time...");
    db.ready(function(api) {
        api.salesforce.lastSync(function(err, doc) {
            if(err) {
                return callback(err);
            }
            lastSyncInfo = doc;

            if (typeof(lastSyncInfo) === 'undefined') {
                // there was no lastSync so it can't be locked
                lastSyncInfo = {
                    locked: false,
                    syncStarted: null,
                    lastSync: null
                };
                callback(null);
            }

            var now = Date.now();
            var hour = 60 * 60 * 1000;
            syncStartDate = now;
            if(lastSyncInfo.locked && lastSyncInfo.syncStarted < (now - hour)) {
                console.log("aborting sync because something else is syncing");
                return callback(new Error("Sync Locked"));
            }
            callback(null);
        });
    });
}

function setSyncLock(callback) {
    console.log("Claiming the sync lock...");
    lastSyncInfo.locked = true;
    lastSyncInfo.syncStarted = Date.now();
    db.ready(function(api) {
        api.salesforce.updateSyncInfo(lastSyncInfo, function(err, newInfo) {
            if(newInfo) {
                lastSyncInfo = newInfo;
            }
            callback(err || null);
        });
    });
}

function setSyncFinished(callback) {
    console.log("Returning the sync lock...");
    lastSyncInfo.locked = false;
    lastSyncInfo.lastSync = syncStartDate;
    db.ready(function(api) {
        api.salesforce.updateSyncInfo(lastSyncInfo, function(err, newInfo) {
            if(newInfo) {
                lastSyncInfo = newInfo;
            }
            callback(err || null);
        });
    });
}

/// get some info out of salesforce before we start
function getIndividualAccountId(callback) {
    console.log("Retrieving Salesforce account information...");
    var query = "SELECT Id FROM Account WHERE Name = 'Individual'";
    sf.query(query, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        if(results && results.records && results.records.length > 0) {
            individualAccountId = results.records[0].Id;
            callback(null, individualAccountId);
        } else {
            callback(new Error("The 'Individual' Account does not exist"));
        }
    });
}

function getRecordTypePublicDonationId(callback) {
    console.log('Retrieving Salesforce type id for public donations...');
    var query = "SELECT Id FROM RecordType WHERE DeveloperName = 'Donation_public'";
    sf.query(query, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        if(results && results.records && results.records.length > 0) {
            recordTypePublicDonationId = results.records[0].Id;
            callback(null, recordTypePublicDonationId);
        } else {
            callback(new Error("The 'Public Donation' RecordType does not exist"));
        }
    });
}

function getRecordTypeContactId(callback) {
    console.log('Retrieving Salesforce type id for contact record type...');
    var query = "SELECT Id FROM RecordType WHERE DeveloperName = 'Contact'";
    sf.query(query, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        if(results && results.records && results.records.length > 0) {
            recordTypeContactId = results.records[0].Id;
            callback(null, recordTypeContactId);
        } else {
            callback(new Error("The 'Contact' RecordType does not exist"));
        }
    });
}

function getBudgetYearCurrent(callback) {
    console.log('Retrieving budget year from Salesforce...');
    var query = "SELECT Id FROM donation_split__Budget_Year__c WHERE End_date__c > TODAY AND Start_date__c < TODAY";
    sf.query(query, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        if(results && results.records && results.records.length > 0) {
            currentBudgetYearId = results.records[0].Id;
            callback(null, currentBudgetYearId);
        } else {
            callback(new Error("Could not find the current budget year"));
        }
    });
}

//// ACCELERATORS
// save loop - EEK!
function saveAccelerator(accelerator, delta, callback) {
    db.ready(function(api) {
        api.accelerator.save(accelerator, function(err) {
            if(err && err.message === 'conflict') {
                api.accelerator.get(accelerator._id, function(err, accel) {
                    if(err) {
                        return callback(err);
                    }
                    mapper.applyDelta(accel, delta);
                    saveAccelerator(accel, delta, callback);
                });
                return;
            }
            if(err) {
                console.log('Error saving accelerator');
                return callback(err);
            }
            callback(null);
        });
    });
}

function syncAccelerator(sfAccelerator, callback) {
    db.ready(function(api) {
        api.accelerator.get(sfAccelerator.Id, function(err, accel) {
            if(err && err.message !== 'missing') {
                console.log('Error getting accelerator');
                return callback(err);
            }
            if(err && err.message === 'missing') {
                accel = api.accelerator.blank();
            }
            var delta = mapper.acceleratorDelta(sfAccelerator, accel);
            mapper.applyDelta(accel, delta);
            saveAccelerator(accel, delta, callback);
        });
    });
}

function syncAccelerators(callback) {
    console.log("Syncing accelerators from Salesforce...");
    var ldate = new Date(lastSyncInfo.lastSync);
    var acceleratorQuery = queries.accelerator + " WHERE LastModifiedDate > " + ldate.toISOString();
    sf.query(acceleratorQuery, oauth, function(err, results) {
        if(err) {
            console.log('Error syncing accelerators');
            return callback(err);
        }
        if(results && results.records) {
            async.whilst(
                function () { return results.records.length !== 0; }, 
                function(cb) { syncAccelerator(results.records.pop(), cb); }, 
                callback);
        } else {
            callback(null);
        }
    });
}

//// PROJECTS
// save loop - EEK!
function saveProject(project, delta, callback) {
    db.ready(function(api) {
        api.project.save(project, function(err) {
            if(err && err.message === 'conflict') {
                api.project.getById(project._id, function(err, proj) {
                    if(err) {
                        return callback(err);
                    }
                    mapper.applyDelta(proj, delta);
                    saveProject(proj, delta, callback);
                });
                return;
            }
            if(err) {
                console.log('Error saving project');
                return callback(err);
            }
            callback(null);
        });
    });
}

function syncProject(sfProject, callback) {
    db.ready(function(api) {
        api.project.getById(sfProject.Id, function(err, proj) {
            if(err && err.message !== 'missing') {
                console.log('Error getting project');
                return callback(err);
            }
            if(err && err.message === 'missing') {
                proj = api.project.blank();
            }
            var delta = mapper.projectDelta(sfProject, proj);
            mapper.applyDelta(proj, delta);
            saveProject(proj, delta, callback);
        });
    });
}

function syncProjects(callback) {
    console.log("Syncing projects from Salesforce...");
    var ldate = new Date(lastSyncInfo.lastSync);
    var projectQuery = queries.project + " WHERE LastModifiedDate > " + ldate.toISOString();
    sf.query(projectQuery, oauth, function(err, results) {
        if(err) {
            console.log('Error syncing projects');
            return callback(err);
        }
        if(results && results.records) {
            async.whilst(
                function () { return results.records.length !== 0; }, 
                function(cb) { syncProject(results.records.pop(), cb); }, 
                callback);
        } else {
            callback(null);
        }
    });
}

function syncUpdatesToProject(projectId, sfUpdates, callback) {
    db.ready(function(api) {
        api.project.getById(projectId, function(err, proj) {
            if(err) {
                return callback(err);
            }
            var delta = mapper.projectUpdatesDelta(sfUpdates, proj);
            mapper.applyDelta(proj, delta);
            saveProject(proj, delta, callback);
        });
    });
}

function syncProjectUpdates(callback) {
    console.log("Syncing project updates...");
    var ldate = new Date(lastSyncInfo.lastSync);
    var query = queries.projectUpdates + " WHERE LastModifiedDate > " + ldate.toISOString();
    sf.query(query, oauth, function(err, results) {
        if(err) {
            console.log('Error syncing project updates');
            return callback(err);
        }
        if(results && results.records) {
            var groupByProject = _.groupBy(results.records, function(rec) {
                return rec.Project__r.Id;
            });
            var projectIds = Object.keys(groupByProject);
            async.whilst(
                function () { return projectIds.length !== 0; }, 
                function(cb) { 
                    var projectId = projectIds.pop();
                    syncUpdatesToProject(projectId, groupByProject[projectId], cb);
                }, 
                callback);
        } else {
            callback(null);
        }
    });
}

function syncFundingToProject(projectId, sfGrants, callback) {
    db.ready(function(api) {
        api.project.getById(projectId, function(err, proj) {
            if(err) {
                return callback(err);
            }
            var delta = mapper.fundingDelta(sfGrants, proj);
            mapper.applyDelta(proj, delta);
            saveProject(proj, delta, callback);
        });
    });
}

function syncFundingGrants(callback) {
    console.log("Syncing funding grants...");
    var ldate = new Date(lastSyncInfo.lastSync);
    var query = queries.designations + " WHERE LastModifiedDate > " + ldate.toISOString();
    sf.query(query, oauth, function(err, results) {
        if(err) {
            console.log('Error syncing grants')
            return callback(err);
        }
        if(results && results.records) {
            var groupByProject = _.groupBy(results.records, function(rec) {
                return rec.Changemaker_Project__r.Id;
            });
            var projectIds = Object.keys(groupByProject);
            async.whilst(
                function () { return projectIds.length !== 0; }, 
                function(cb) { 
                    var projectId = projectIds.pop();
                    syncFundingToProject(projectId, groupByProject[projectId], cb);
                }, 
                callback);
        } else {
            callback(null);
        }
    });
}

function updateFundingAmountsForProject(projectId, fundingGrantAmounts, callback) {
    db.ready(function(api) {
        api.project.getById(projectId, function(err, proj) {
            if(err) {
                return callback(err);
            }
            var shouldSave = false;
            proj.funding_grants.forEach(function(grant) {
                var val = fundingGrantAmounts[grant.salesforce_ref] || 0;
                if(grant.received_usd === undefined || grant.received_usd !== val) {
                    grant.received_usd = val;
                    shouldSave = true;
                }
            });
            if(shouldSave) {
                return api.project.save(proj, callback);
            }
            callback(null);
        });
    });
}

function updateFundingGrantAmounts(callback) {
    // TODO: this will need to do some sort of paging when we get past 2000 
    // designations
    var query = "SELECT SUM(donation_split__Amount__c), donation_split__Designation__c FROM donation_split__Designation_Budget__c GROUP BY donation_split__Designation__c";
    sf.query(query, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        if(results && results.records) {
            var fundingGrantAmounts = { };
            results.records.forEach(function(rec) {
                fundingGrantAmounts[rec.donation_split__Designation__c] = rec.expr0;
            });
            db.ready(function(api) {
                api.project.getIds(function(err, projectIds) {
                    if(err) {
                        return callback(err);
                    }
                    async.whilst(
                        function () { return projectIds.length !== 0; }, 
                        function(cb) { 
                            var projectId = projectIds.pop();
                            updateFundingAmountsForProject(projectId, fundingGrantAmounts, cb);
                        }, 
                        callback);
                    });
            });
        } else {
            callback(null);
        }
    });
}

//SELECT SUM(donation_split__Amount__c), donation_split__Designation__c FROM donation_split__Designation_Budget__c GROUP BY donation_split__Designation__c

//// USERS
// save loop - EEK!
function saveUser(user, delta, callback) {
    db.ready(function(api) {
        api.user.save(user, function(err) {
            if(err && err.message === 'conflict') {
                api.user.get(user._id, function(err, usr) {
                    if(err) {
                        return callback(err);
                    }
                    mapper.applyDelta(usr, delta);
                    saveUser(usr, delta, callback);
                });
                return;
            }
            if(err) {
                return callback(err);
            }
            callback(null);
        });
    });
}

function syncSFUser(sfUser, callback) {
    db.ready(function(api) {
        var email = sfUser.Email;
        if(!email) {
            return callback(null);
        }
        api.user.getByEmail(email, function(err, user) {
            if(err && err.message !== 'missing') {
                return callback(err);
            }
            if(!user) {
                user = api.user.blank();
            }
            var delta = mapper.userDelta(sfUser, user);
            if(Object.keys(delta).length === 0) {
                return callback(null);
            }
            mapper.applyDelta(user, delta);
            saveUser(user, delta, callback);
        });
    });
}

function syncUsersFromSF(callback) {
    console.log("    from Salesforce...");
    var ldate = new Date(lastSyncInfo.lastSync);
    // grab users from sales force and bring them down
    var userQuery = queries.people + " WHERE LastModifiedDate > " + ldate.toISOString();
    //console.log(userQuery);
    sf.query(userQuery, oauth, function(err, results) {
        if(err) {
            return callback(err);
        }
        //console.log("results %j", results);
        if(results && results.records) {
            async.whilst(
                function () { return results.records.length !== 0; }, 
                function(cb) { syncSFUser(results.records.pop(), cb); }, 
                callback);
        } else {
            callback(null);
        }
    });
}

function upsertSFUser(user, callback) {
    var upsertParams = {};
    upsertParams.FirstName = user.first_name;
    upsertParams.LastName = user.last_name;
    upsertParams.Email = user.email;
    upsertParams.RecordTypeId = recordTypeContactId;
    var method = "insert";
    if(user.salesforce_ref) {
        upsertParams.Id = user.salesforce_ref;
        method = "update";
    } else {
        upsertParams.AccountId = individualAccountId;
    }
    var newuser = nforce.createSObject('Contact', upsertParams);
    sf[method](newuser, oauth, function(err, resp) {
        if(err) {
            console.error(err);
            return callback(err);
        }
        if(method === 'insert') {
            user.salesforce_ref = resp.id;
            return saveUser(user, {salesforce_ref : resp.id}, callback);
        }
        callback(null, resp);
    });
}

function syncUsersFromCouch(callback) {
    console.log("    from CouchDB...");
    var ldate = lastSyncInfo.lastSync;
    db.ready(function(api) {
        api.user.getByLastModified(ldate, function(err, users) {
            if(err) {
                return callback(err);
            }
            if(users.length === 0) {
                return callback(null);
            }
            // 3 cases here
            // 1. we have a salesforce_ref so we know who
            // 2. we have an email address, but we have to check if they are in SF
            // 3. they don't exist in SF
            // for now we're just going to merge all three and do an upsert
            async.whilst(
                function () { return users.length !== 0; }, 
                function(cb) { 
                    var kvp = users.pop(); 
                    upsertSFUser(kvp.value, cb); 
                }, 
                callback);
        });
    });
}

function syncUsers(callback) {
    console.log("Syncing users...");
    async.series([syncUsersFromCouch, syncUsersFromSF], callback);
}

/// DONATIONS
function syncDonation_stepOne(donationTrail, donation, user, callback) {
    // step 1 make an opportunity
    // - this is the total value of the donation
    if(donationTrail.opportunity_ref) {
        return callback(null, donationTrail);
    }
    var userName = user.first_name + ' ' + user.last_name;
    var oppName = 'Web donation from ' + userName + ' - ' + donationTrail._id;
    var opportunityValues = {
        "StageName": "Closed Won",
        "LeadSource": "Web",
        'Name': oppName,
        "Amount": donation.amount_usd,
        "CloseDate": donation.transaction_date,
        "RecordTypeId": recordTypePublicDonationId,
        "AccountId": individualAccountId
    };
    var opp = nforce.createSObject('Opportunity', opportunityValues);
    sf.insert(opp, oauth, function(err, result) {
        if(err) {
            return callback(err);
        }
        donationTrail.opportunity_ref = result.id;
        db.ready(function(api) {
            api.donation.save(donationTrail, callback);
        });
    });
}

function syncDonation_stepTwo(donationTrail, userRef, callback) {
    // step 2 assign the donation to a person/contact
    if(donationTrail.opportunity_contact_ref) {
        return callback(null, donationTrail);
    }
    var oppContactRoleValues = {
        "OpportunityId": donationTrail.opportunity_ref,
        "ContactId": userRef,
        "Role": "donor",
        "IsPrimary": "true"
    };
    var oppContactRole = nforce.createSObject('OpportunityContactRole', oppContactRoleValues);
    sf.insert(oppContactRole, oauth, function(err, result) {
        if(err) {
            return callback(err);
        }
        donationTrail.opportunity_contact_ref = result.id;
        db.ready(function(api) {
            api.donation.save(donationTrail, callback);
        });
    });
}

function syncDonation_stepThree(donationTrail, donation, fundingGrantRef, callback) {
    // step 3 create a designation budget record
    //        this assigns the money to a project
    if(donationTrail.designation_ref) {
        return callback(null, donationTrail);
    }
    var designationVals = {
        "donation_split__Amount__c": donation.amount_usd,
        "donation_split__Budget_Year__c": currentBudgetYearId,
        "donation_split__Designation__c": fundingGrantRef,
        "donation_split__Opportunity__c": donationTrail.opportunity_ref
    };
    var designationBudget = nforce.createSObject('donation_split__Designation_Budget__c', designationVals);
    sf.insert(designationBudget, oauth, function(err, result) {
        if(err) {
            return callback(err);
        }
        donationTrail.designation_ref = result.id;
        db.ready(function(api) {
            api.donation.save(donationTrail, callback);
        });
    });
}

function syncDonation(donation, fundingGrantRef, callback) {
    db.ready(function(api) {
        var donationId = "braintree_" + donation.transaction.id;
        api.donation.get(donationId, function(err, donationTrail) {
            if(err && err.message !== 'missing') {
                return callback(err);
            }
            if(!donationTrail) {
                donationTrail = { 
                    _id: donationId,
                    type: "donationInfo"
                };
            }
            api.user.get(donation.user_id, function(err, user) {
                if(err) {
                    return callback(err);
                }
                if(!user) {
                    return callback(new Error("user not found"));
                }
                var userRef = user.salesforce_ref;
                syncDonation_stepOne(donationTrail, donation, user,
                    function(err, dtrail1) {
                        if(err) {
                            return callback(err);
                        }
                        syncDonation_stepTwo(dtrail1, userRef, 
                            function(err, dtrail2) {
                                if(err) {
                                    return callback(err);
                                }
                                syncDonation_stepThree(dtrail2, donation, fundingGrantRef, callback);
                            });
                    });
            });
        });
    });
}

function syncDonationsInProject(projectId, callback) {
    db.ready(function(api) {
        api.project.getById(projectId, function(err, proj) {
            if(err) {
                return callback(err);
            }
            var ldate = lastSyncInfo.lastSync;
            var work = [];
            proj.funding_grants.forEach(function(grant) {
                grant.contributions.forEach(function(contrib) {
                    if(contrib.transaction_date < ldate) {
                        return;
                    }
                    if(!contrib.transaction || !contrib.transaction.id) {
                        console.log("skipping contribution because it has no transaction data: %j", contrib);
                        return;
                    }
                    work.push([contrib, grant.salesforce_ref]);
                });
            });
            if(work.length === 0) {
                callback(null);
            }
            async.whilst(
                function () { return work.length !== 0; }, 
                function(cb) { 
                    var item = work.pop(); 
                    syncDonation(item[0], item[1], cb); 
                },
                callback);
        });
    });
}

function syncAllDonations(callback) {
    console.log('Syncing donations...');
    var ldate = lastSyncInfo.lastSync;
    db.ready(function(api) {
        api.project.getIdsForNewContributions(ldate, function(err, projectIds) {
            async.whilst(
                function () { return projectIds.length !== 0; }, 
                function(cb) { 
                    var projectId = projectIds.pop();
                    syncDonationsInProject(projectId, cb);
                }, 
                callback);
        });
    });
    
}

function startSync() {
    console.log("Started Sync");
    async.series([
        getLastSyncDate,
        // lock
        setSyncLock,
        // useful things to know
        getIndividualAccountId,
        getRecordTypePublicDonationId,
        getRecordTypeContactId,
        getBudgetYearCurrent,
        // sync users
        syncUsers,
        // sync accelerators
        syncAccelerators,
        // sync projects
        syncProjects,
        syncFundingGrants,
        updateFundingGrantAmounts,
        syncProjectUpdates,
        // sync donations
        syncAllDonations,
        // unlock
        setSyncFinished], function(err, objs) {
            if(err) console.error(err);
            if(objs) console.log(JSON.stringify(objs));
            console.log("Ended Sync");
        });
}

login.connect(function(err, connectionDetails) {
    if(err) {throw err;}
    sf = connectionDetails.nforce;
    oauth = connectionDetails.oauth;
    startSync();
});
