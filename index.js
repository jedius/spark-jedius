"use strict";

var http = require('http');
var config = require("./config");

var app = require('./app').getConfiguredApp();

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
  if(config.environment !== "production") {
    console.log("Environment not set to production!");
  }
});


var spawn = require('child_process').spawn;
var syncTime = 60 * 60 * 1000; // 1hr
function runSync() {
	var syncer = spawn('node', ['index.js'], { 
		cwd: __dirname + "/sync",
		env: process.env
	});
	syncer.stdout.on('data', function(d) {
		console.log("sync: " + d.toString());
	});
	syncer.stderr.on('data', function(d) {
		console.log("sync: " + d.toString());
	});
	syncer.on('close', function() {
		setTimeout(runSync, syncTime);
	});
}
// Don't run the initial sync immediately. If our server crashes, we don't
// want to compound the error by syncing constantly.
var initialDelay = 30 * 1000; // 30s
setTimeout(runSync, initialDelay);
