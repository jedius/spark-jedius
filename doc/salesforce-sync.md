Salesforce Syncing
==================

The basic workflow is

1. Get Sync Lock
2. Sync users from Salesforce
3. Sync users from couch
4. Sync projects from Salesforce
5. Sync funding grants from Salesforce
6. Release Sync Lock


Sync Schedule
-------------

The sync process is scheduled through the webserver index.js - The webserver 
will spawn a child process to perform the sync every hour (configurable) 
starting from launch of the application. The idea with this is that we don't
need to setup a cron job or run a separate box on Heroku to deal with this. To
overcome the fact that multiple webservers are running and we only want one 
process syncing at a time we add the sync lock. The lock prevents multiple 
processes syncing to the one couchdb at the same time.


Structure of Sync Folder
------------------------

 * index.js - this is where the sync process runs from
 * makequeries.js - this creates a SELECT * query for the tables that we are 
    interested in. Doing this rather than hand writing the query because it's
    easier to see the fields and their names via a SELECT * than to use salesforce
 * mapper.js - This creates deltas between salesforce and couch and then applies
    those deltas.
 * queries.json - produced by makequeries.js
 * login.js - the underlying module that deals with connecting to salesforce
 * runquery.js - run a query in queries.json and print the output, useful for debug


Sync users from Salesforce
--------------------------

This does a select from the 'Contact' table in salesforce to find records that
have been modified since the last sync. Right now we're only syncing first name,
last name and email address. The record stays private for now since we're not 
sure how this will effect companies that have been added to salesforce. Matching
is done on email address.


Sync users from couchdb
-----------------------

This selects all Users that have been updated since the last sync and either
inserts or updates the salesforce record. The way we determine if it's an insert
or update is whether or not we have the salesforce reference.


Sync projects from Salesforce
-----------------------------

Project sync is split into two parts. The first part brings down the projects. 
The second part brings down the funding grants. The reason for this is that they
are in two tables in Salesforce. 

