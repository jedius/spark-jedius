"use strict";

var hasher = require('../../lib/hasher');


// make sure there is "enough" entropy in our salt creator
exports.saltCreationTest = function(test) {
    var salts = [];
    for(var k=0; k<100; k++) {
        salts.push(hasher.createRandomSalt());
    }
    salts.sort();
    for(var i=1; i<100; i++) {
        test.notEqual(salts[i-1], salts[i]);
    }
    test.done();
};

// make sure that each attempt at hashing is unqiue
exports.uniqueHashingTest = function(test) {
    var attempt1 = hasher.compute("password");
    var attempt2 = hasher.compute("password");
    test.notDeepEqual(attempt1, attempt2);
    test.done();
};

exports.checkPasswordTest = function(test) {
    var password = "passw0rd";
    var hashObj = hasher.compute(password);
    var check = hasher.computeWithSalt(password, hashObj.iterations, hashObj.salt);
    test.equal(hashObj.hash, check.hash);
    test.done();
};