
# spark.org.au

## Important Documents

* [Coding Standards](doc/coding-standards.md)
* [Directory Structure](doc/directory-structure.md)

## Setting up a dev environment

1. Download and install [Git](http://git-scm.com/)
2. Download and install [NodeJS](http://nodejs.org/) and NPM (should come with Node)
3. Install Grunt v0.3.17 globally (a consistent version between developers would be helpful)

    ```
    > npm install -g grunt@0.3.17
    ```

4. Clone this repository

    ```
    > git clone git@bitbucket.org:sparkintl/spark.org.au.git
    ```

5. Download all the dependencies for the repo:

    ```
    > cd spark.org.au
    > npm install
    ```

6. Implement the Facebook environment variables on your machine - see
facebook.md in DropBox for keys

    ```
    > export FB_APP_ID=XXXXXXXXXXX
    ```

    ```
    > export FB_APP_SECRET=XXXXXXXXXXXXXXXXXX
    ```

    Or add it to your bash profile.

7. Install [CouchDB](http://couchdb.apache.org/) and [redis](http://redis.io/download)

    If you're running [Homebrew on OSX](http://github.com/mxcl/homebrew) then you can :

    ```
    > brew install couchdb
    > brew install redis
    ```

And you're set!

## Tools Used

* [ExpressJS](http://expressjs.com/)
* [Jade-lang](http://jade-lang.com/)
* [Stylus](http://learnboost.github.com/stylus/)


## Running tests

To run the client-side QUnit tests, download [PhantomJS](http://phantomjs.org/) and add it to your `PATH`

If you're running [Homebrew on OSX](http://github.com/mxcl/homebrew) then you can just `brew install phantomjs`

### Lint and run tests

```
> grunt
```

### Watch for changes and automatically run linting and tests

*You should develop with a watch task running in the background. You will be much more productive.*

```
> grunt watch
```

### Some other handy hints

There are several environment variables utilised - these include :

* `environment: process.env.NODE_ENV || "development"`
* (Session) `secret: process.env.SESSION_SECRET ||
"purplemonkeydishwasherbatteryacid"`
* `couchdb: process.env.COUCHDB || 'http://localhost:5984'`
* (Facebook) `clientID: process.env.FB_APP_ID`
* (Facebook) `clientSecret: process.env.FB_APP_SECRET`

You shouldn't need to worry about many of these in development. See above for Facebook variable instructions.

### Rules of this repo

* Add QUnit/Nodeunit tests for any new or changed functionality.
* Every change requires a pull request to be approved by another contributor.
* No change will be accepted with failing JSHint or failing tests
* Don't commit any change that breaks `grunt watch`.

## Release History
_(Nothing yet)_

## License
Copyright (c) 2012 Spark International
